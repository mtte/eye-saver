package me.mtte.eyesaver;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class EyeSaverUi extends JDialog {

	private static final long serialVersionUID = 1L;

	private static final SimpleDateFormat SECONDS_FORMAT = new SimpleDateFormat("ss");

	private JLabel timeLabel;
	private JLabel unitLabel;
	private JButton mainBtn;
	
	private Timer timer;
	
	private int runs = EyeSaver.SAVE_TIME_SECONDS;
	
	public EyeSaverUi() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setAlwaysOnTop(true);
		setLayout(new BorderLayout(20, 0));
		setModalityType(ModalityType.APPLICATION_MODAL);
		setResizable(false);
		setTitle("Eye Saver");
		setIconImage(EyeSaver.getEyeSaverImage());
		setBounds();
		
		initUi();
		
		setVisible(true);
	}
	
	private void setBounds() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width * 2 - 250, screenSize.height - 130, 250, 130);
	}
	
	private void initUi() {
		this.mainBtn = new JButton("Start");
		this.mainBtn.setPreferredSize(new Dimension(90, 90));
		this.mainBtn.addActionListener(this::startCountDown);
		this.add(mainBtn, BorderLayout.EAST);
		
		this.timeLabel = new JLabel(String.valueOf(EyeSaver.SAVE_TIME_SECONDS));
		this.timeLabel.setFont(this.timeLabel.getFont().deriveFont(75.0f));
		this.timeLabel.setVerticalAlignment(JLabel.BOTTOM);
		this.unitLabel = new JLabel("<html>seconds<br/>&nbsp;</html>");
		this.unitLabel.setVerticalAlignment(JLabel.BOTTOM);
		
		JPanel container = new JPanel(new BorderLayout());
		container.add(this.timeLabel, BorderLayout.CENTER);
		container.add(this.unitLabel, BorderLayout.EAST);
		this.add(container, BorderLayout.CENTER);
	}
	
	private void startCountDown(ActionEvent e) {
		this.mainBtn.setEnabled(false);
		this.timer = new Timer(1000, this::countDown);
		this.timer.start();
	}
	
	private void countDown(ActionEvent e) {
		this.runs--;
		
		String time = SECONDS_FORMAT.format(this.runs * 1000);
		this.timeLabel.setText(time);
		
		if (this.runs < 0) {
			this.timer.stop();
			EyeSaverUi.this.dispose();
		}
	}
	
}
