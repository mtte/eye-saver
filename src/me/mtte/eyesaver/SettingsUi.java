package me.mtte.eyesaver;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import net.miginfocom.layout.CC;
import net.miginfocom.swing.MigLayout;

public class SettingsUi extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private static final int WINDOW_WIDTH = 600;
	private static final int WINDOW_HEIGHT = 400;
	
	public SettingsUi() {
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Settings - Eye Saver");
		setIconImage(EyeSaver.getEyeSaverImage());
		setBounds();
		
		initUi();
		
		setVisible(true);
	}
	
	private void setBounds() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(screenSize.width / 2 - (WINDOW_WIDTH / 2), screenSize.height / 2 - (WINDOW_HEIGHT / 2), WINDOW_WIDTH, WINDOW_HEIGHT);
	}
	
	private void initUi() {
		this.setLayout(new MigLayout("wrap, debug, fillX", "[]rel[grow]", "[]10[]"));
		
		JLabel title = new JLabel("Settings", JLabel.CENTER);
		title.setFont(title.getFont().deriveFont(25.0f));
		
		JLabel intervalLabel = new JLabel("Interval Time:");
		intervalLabel.setToolTipText("The time in minutes between invocations of the reminder.");
		SpinnerNumberModel intervalModel = new SpinnerNumberModel(Settings.INSTANCE.getIntervalTimeMinutes(), 0, null, 1);
		JSpinner intervalSpinner = new JSpinner(intervalModel);
		intervalLabel.setLabelFor(intervalSpinner);
		
		JLabel saveTimeLabel = new JLabel("Save Time:");
		saveTimeLabel.setToolTipText("How long the save count down is in seconds.");
		SpinnerNumberModel saveTimeModel = new SpinnerNumberModel(Settings.INSTANCE.getIntervalTimeMinutes(), 0, null, 1);
		JSpinner saveTimeSpinner = new JSpinner(saveTimeModel);
		intervalLabel.setLabelFor(saveTimeSpinner);
		
		this.add(title, new CC().growX().spanX());
		this.add(new JLabel(), new CC().wrap());
		this.add(intervalLabel);
		this.add(intervalSpinner, new CC().growX());
		this.add(saveTimeLabel);
		this.add(saveTimeSpinner, new CC().growX());
		this.add(new JPanel(), new CC().grow().wrap());
		
//		this.add(new JButton("test"), new CC().alignX("right").);
		
	}
	
}
