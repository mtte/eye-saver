package me.mtte.eyesaver;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public final class EyeSaver {
	
	public static final int INTERVAL_TIME_MINUTES = 20;
	public static final int SAVE_TIME_SECONDS = 20;
	
	private static TrayIcon trayIcon;
	private static ScheduledFuture<?> future;
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		
		Runtime.getRuntime().addShutdownHook(new Thread(EyeSaver::cleanupRessources));
		
		initSystemTray();
		
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		future = executor.scheduleWithFixedDelay(EyeSaverUi::new, INTERVAL_TIME_MINUTES, INTERVAL_TIME_MINUTES, TimeUnit.MINUTES);
		
		new SettingsUi();
	}
	
	private static void initSystemTray() {
		MenuItem quitItem = new MenuItem("Quit");
		MenuItem settingsItem = new MenuItem("Settings");
		MenuItem showNow = new MenuItem("Show Now");
		
		quitItem.addActionListener(e -> exitApplication());
		settingsItem.addActionListener(e -> new SettingsUi());
		showNow.addActionListener(e -> new EyeSaverUi());
		
		PopupMenu popup = new PopupMenu();
		popup.add(settingsItem);
		popup.add(showNow);
		popup.addSeparator();
		popup.add(quitItem);
		
		trayIcon = new TrayIcon(getEyeSaverImage());
		trayIcon.setToolTip("Double click to quit");
		trayIcon.addActionListener(e -> exitApplication());
		trayIcon.setPopupMenu(popup);
		
		try {
			SystemTray.getSystemTray().add(trayIcon);
		} catch (AWTException e) {
			System.out.println("Tray Icon could not be added");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public static void cleanupRessources() {
		if (trayIcon != null) {
			SystemTray.getSystemTray().remove(trayIcon);
		}
		if (future != null) {
			future.cancel(true);			
		}
	}
	
	private static void exitApplication() {
		cleanupRessources();
		System.exit(0);
	}
	
	public static Image getEyeSaverImage() {
		URL imageUrl = EyeSaver.class.getResource("eye-saver.png");
		ImageIcon icon = new ImageIcon(imageUrl, "tray icon");
		return icon.getImage();
	}
	
}
