package me.mtte.eyesaver;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public enum Settings {

	INSTANCE;
	
	private static final String CONFIG_FILE = "eye-saver.properties";
	
	private static final String SAVE_TIME_SECONDS = "save-time-seconds";
	private static final String INTERVAL_TIME_MINUTES = "interval-time-minutes";
	
	private Properties properties = new Properties();
	
	private Settings() {
		try {
			if (Files.exists(Paths.get(CONFIG_FILE))) {
				this.properties.load(new FileInputStream(CONFIG_FILE));				
			}
		} catch (IOException e) {
			System.out.println("Could not load config file");
			e.printStackTrace();
			EyeSaver.cleanupRessources();
			System.exit(1);
		}
	}
	
	private void save() {
		try {
			this.properties.store(new FileOutputStream(CONFIG_FILE), "Eye Saver Config");
		} catch (IOException e) {
			System.out.println("Could not save config file");
			e.printStackTrace();
		}
	}
	
	public int getSaveTimeSeconds() {
		String value = this.properties.getProperty(SAVE_TIME_SECONDS, "20");
		return Integer.parseInt(value);
	}
	
	public void setSaveTimeSeconds(int saveTimeSeconds) {
		this.properties.setProperty(SAVE_TIME_SECONDS, String.valueOf(saveTimeSeconds));
		save();
	}
	
	public int getIntervalTimeMinutes() {
		String value = this.properties.getProperty(INTERVAL_TIME_MINUTES, "20");
		return Integer.parseInt(value);
	}
	
	public void setIntervalTimeMinutes(int intervalTimeMinutes) {
		this.properties.setProperty(INTERVAL_TIME_MINUTES, String.valueOf(intervalTimeMinutes));
		save();
	}
	
}
